
import { Row, Col, Card, Button, Container } from 'react-bootstrap';
import { Link } from 'react-router-dom'

export default function LandingPage(props) {
    const { cart, onAdd} = props;
	return (
    <Container class='text-center'>
        <Row  className = 'mt-3 mb-3'>
            <Col xs= {12} md={4} class= 'mb-3'>				
                <Card style={{ width: '18rem' }} className= 'cardDetails p-3'>
                    <Card.Img variant="top" src="../images/ceravemoisture.jpg" height='300' />
                    <Card.Body>
                        <Card.Title>Cerave Moisturizer</Card.Title>
                   <Link to='/cart'>
                   <Button variant="primary" onClick={() => onAdd(cart)}>Add To Cart</Button>
                       </Link>     
                    
                    </Card.Body>
                </Card>
			</Col>
			<Col xs= {12} md={4} class= 'mb-3'>				
                <Card style={{ width: '18rem' }} className= 'cardDetails p-3'>
                    <Card.Img variant="top" src="../images/eyebrowpencil.jpg" height='300' />
                    <Card.Body>
                        <Card.Title>Eyebrow pencil</Card.Title>
                        
                        <Link to='/cart'>
                   <Button variant="primary" onClick={() => onAdd(cart)}>Add To Cart</Button>
                       </Link> >
                    </Card.Body>
                </Card>
			</Col>
			<Col xs= {12} md={4} class= 'mb-3'>				
                <Card style={{ width: '18rem' }} className= 'cardDetails p-3'>
                    <Card.Img variant="top" src="../images/hairmask.jpg" height='300' />
                    <Card.Body>
                        <Card.Title>Hair Mask</Card.Title>
                        
                        <Link to='/cart'>
                   <Button variant="primary" onClick={() => onAdd(cart)}>Add To Cart</Button>
                       </Link> 
                    </Card.Body>
                </Card>
			</Col>
        </Row>
        <Row  className = 'mt-3 mb-3'>
            <Col xs= {12} md={4} class= 'mb-3'>				
                <Card style={{ width: '18rem' }} className= 'cardDetails p-3'>
                    <Card.Img variant="top" src="../images/blushon.jpg" height='300' />
                    <Card.Body>
                        <Card.Title>Blush On</Card.Title>
                        
                        <Link to='/cart'>
                   <Button variant="primary" onClick={() => onAdd(cart)}>Add To Cart</Button>
                       </Link> 
                    </Card.Body>
                </Card>
			</Col>
			<Col xs= {12} md={4} class= 'mb-3'>				
                <Card style={{ width: '18rem' }} className= 'cardDetails p-3'>
                    <Card.Img variant="top" src="../images/cccushion.jpg" height='300' />
                    <Card.Body>
                        <Card.Title>CC Cushion SPF45</Card.Title>
                        
                        <Link to='/cart'>
                   <Button variant="primary" onClick={() => onAdd(cart)}>Add To Cart</Button>
                       </Link> 
                    </Card.Body>
                </Card>
			</Col>
			<Col xs= {12} md={4} class= 'mb-3'>				
                <Card style={{ width: '18rem' }} className= 'cardDetails p-3'>
                    <Card.Img variant="top" src="../images/hairgrowth.jpg" height='300' />
                    <Card.Body>
                        <Card.Title>Hair Growth</Card.Title>
                        
                        <Link to='/cart'>
                   <Button variant="primary" onClick={() => onAdd(cart)}>Add To Cart</Button>
                       </Link> 
                    </Card.Body>
                </Card>
			</Col>
        </Row>
		<Row  className = 'mt-3 mb-3'>
            <Col xs= {12} md={4} class= 'mb-3'>				
                <Card style={{ width: '18rem' }} className= 'cardDetails p-3'>
                    <Card.Img variant="top" src="../images/hairmoisturizer.jpg" height='300' />
                    <Card.Body>
                        <Card.Title>Hair Moisturizer</Card.Title>
                        
                        <Link to='/cart'>
                   <Button variant="primary" onClick={() => onAdd(cart)}>Add To Cart</Button>
                       </Link>
                       </Card.Body> 
                </Card>
			</Col>
			<Col xs= {12} md={4} class= 'mb-3'>				
                <Card style={{ width: '18rem' }} className= 'cardDetails p-3'>
                    <Card.Img variant="top" src="../images/hairtreat.jpg" height='300' />
                    <Card.Body>
                        <Card.Title>Hair Treatment</Card.Title>
                        
                        <Link to='/cart'>
                   <Button variant="primary" onClick={() => onAdd(cart)}>Add To Cart</Button>
                       </Link> 
                    </Card.Body>
                </Card>
			</Col>
			<Col xs= {12} md={4} class= 'mb-3'>				
                <Card style={{ width: '18rem' }} className= 'cardDetails p-3'>
                    <Card.Img variant="top" src="../images/jojobaoil.jpg" height='300' />
                    <Card.Body>
                        <Card.Title>Jojoba Oil</Card.Title>
                        
                        <Link to='/cart'>
                   <Button variant="primary" onClick={() => onAdd(cart)}>Add To Cart</Button>
                       </Link> >
                    </Card.Body>
                </Card>
			</Col>
        </Row>
    </Container>
        
     
	)
}