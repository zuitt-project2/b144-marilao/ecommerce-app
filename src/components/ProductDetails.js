import PropTypes from 'prop-types';
import { Card, Button} from 'react-bootstrap';
import { Link } from 'react-router-dom';

export default function ProductDetails({productProp}) {

	const {_id, title, desc, price, img } = productProp;

    return (
        <Card className= 'mb-2'>
           <Card.Body >
               <Card.Title>{title}</Card.Title>
               <Card.image src={img}></Card.image>
               <Card.Subtitle>Description:</Card.Subtitle>
               <Card.Text>{desc}</Card.Text>
               <Card.Subtitle>Price:</Card.Subtitle>
               <Card.Text>PhP {price} </Card.Text>
               <Link className="btn btn-primary" to={`/products/${_id}`}>Details</Link>
           </Card.Body>
       </Card> 
    )
}