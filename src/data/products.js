import img1 from '../images/olay.jpg'
import img2 from '../images/presspowder.jpg'
import img3 from '../images/blushon.jpg'
import img4 from '../images/cccushion.jpg'
import img5 from '../images/ceravemoisturize.jpg'

const productData = [
	{
		id: 1,
		img: img1,
		title: 'Olay',
		desc: '',
		price: 400,
	},
	{
		id: 2,
		img: img2,
		title: 'presspowder',
		desc: '',
		price: 200,
	},
	{
		id: 3,
		img: img3,
		title: 'Blushon',
		desc: '',
		price: 150,
	},
	{
		id: 4,
		img: img4,
		title: 'CC cushion SPF45',
		desc: '',
		price: 600,
	},
	{
		id: 5,
		img: img5,
		title: 'Cerave Moisturizer',
		desc: '',
		price: 1200,
	},
]