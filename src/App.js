import 'bootstrap/dist/css/bootstrap.min.css';
import { useState, useEffect } from 'react';
import { Container } from 'react-bootstrap';
import { BrowserRouter as Router } from 'react-router-dom';
import { Route, Switch } from 'react-router-dom';
import AppNavbar from './components/AppNavbar';
import Products from './pages/Products';
import ProductView from './components/ProductView';
import Home from './pages/Home';
import Cart from './pages/Cart';
import Register from './pages/Register';
import Login from './pages/Login';
import Logout from './pages/Logout';
// import Err from './pages/Err';
import './App.css';
import { UserProvider } from './UserContext';
import 'bootstrap/dist/css/bootstrap.min.css';

function App() {
  // State hook for the user state that's defined for a global scope
  // Initialize an object with properties from the localStorage
  const [ user, setUser ] = useState({
    //email: localStorage.getItem('email')
    id: null,
    isAdmin: null
  })

  // Function for clearing localStorage on Logout
  const unsetUser = () => {
    localStorage.clear();
  }

// Used to check if the user info is properly stored upon login and the localStorage information is cleared upon logout
useEffect(() => {
  console.log(user);
  console.log(localStorage);
},[user])

  return (
  // order of components matter
  <UserProvider value={{user, setUser, unsetUser}}>
    <Router>
      <AppNavbar />
      <Container>
        <Switch>
          <Route exact path = '/' component = {Home} />
          <Route exact path = '/cart' component = {Cart} />
          <Route exact path = '/products' component = {Products} />
          <Route exact path = '/products/:productId' component = {ProductView} />
          <Route exact path = '/login' component = {Login} />
          <Route exact path = '/logout' component = {Logout} />
          <Route exact path = '/register' component = {Register} />
          
        </Switch>
      </Container>
    </Router> 
  </UserProvider>
  );
}

export default App;


// import Header from './components2/Header';
// import Main from './components2/Main';
// import Basket from './components2/Basket';
// import data from './components2/data';
// import { useState } from 'react';
// import { Router, Switch, Route } from 'react-router-dom';
// import Login from './pages2/Login';
// import Register from './pages2/Register';

// function App() {
//   const { products } = data;
//   const [cartItems, setCartItems] = useState([]);
//   const onAdd = (product) => {
//     const exist = cartItems.find((x) => x.id === product.id);
//     if (exist) {
//       setCartItems(
//         cartItems.map((x) =>
//           x.id === product.id ? { ...exist, qty: exist.qty + 1 } : x
//         )
//       );
//     } else {
//       setCartItems([...cartItems, { ...product, qty: 1 }]);
//     }
//   };
//   const onRemove = (product) => {
//     const exist = cartItems.find((x) => x.id === product.id);
//     if (exist.qty === 1) {
//       setCartItems(cartItems.filter((x) => x.id !== product.id));
//     } else {
//       setCartItems(
//         cartItems.map((x) =>
//           x.id === product.id ? { ...exist, qty: exist.qty - 1 } : x
//         )
//       );
//     }
//   };
//   return (
//     <div className="App">
//     <Router>
//       <Switch>
//       <Route exact path = '/login' component = {Login} />
//       <Route exact path = '/' component = {Header} />
//       <Route exact path = '/register' component = {Register} />
//       </Switch>
//     </Router>
    
//       <Header countCartItems={cartItems.length}></Header>
//       <div className="row">
//         <Main products={products} onAdd={onAdd}></Main>
//         <Basket
//           cartItems={cartItems}
//           onAdd={onAdd}
//           onRemove={onRemove}
//         ></Basket>
//       </div>
//     </div>
//   );
// }

// export default App;